--=================================================================================================--
--#################################################################################################--
--=================================================================================================--
-------------------------------------------------------------------------------
-- Title      : PHYSICAL LAYER CONNECTIONS 
-- Project    : TTC-PON 2015
-------------------------------------------------------------------------------
-- File       : TTC_PHY_LAYER.vhd
-- Author     : Jubin MITRA (jmitra@cern.ch)
-- Company    : VECC
-- Created    : 14-03-2016
-- Last update: 14-03-2016
-- Platform   : 
-- Standard   : VHDL'93/08                  
-- Revision   : 1.0                                                     
-- Target Device:         Altera Arria 10                                                          
-- Tool version:          Quartus II 15.1                                                                
-------------------------------------------------------------------------------
-- Description: 
-- It connects the Tx and Rx native phy with the Transceiver PLL along with PHY reset								
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 14-03-2016  1.0      Jubin	Created
-------------------------------------------------------------------------------
--=================================================================================================--
--#################################################################################################--
--=================================================================================================--

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

use work.TTC_PON_PACKAGE.all;
--=================================================================================================--
--#######################################   Entity   ##############################################--
--=================================================================================================--
entity TTC_PHY_LAYER is 
port (
-- RESET
        GENERAL_RESET_I                      : in std_logic;
        TX_PHY_RESET_I                       : in std_logic;
        RX_PHY_RESET_I                       : in std_logic;
        ATX_PLL_RESET_I                      : in std_logic;
        
-- CLOCKS
  		XCVR_REF_CLK_I                       : in  std_logic;
        USER_LOGIC_CLK_I                     : in  std_logic;
        TX_PMA_WORD_CLK_O                    : out std_logic;
        RX_PMA_WORD_CLK_O                    : out std_logic;
        
-- User Data Bus Lines
        XCVR_PHY_I                           : in  phy_signals_i;
        XCVR_PHY_O                           : out phy_signals_o
);
end entity TTC_PHY_LAYER;


--=================================================================================================--
--####################################   Architecture   ###########################################-- 
--=================================================================================================--

architecture mixed of TTC_PHY_LAYER is

	--==================================== Constant Definition ====================================--   
	

	--==================================== Signal Definition ======================================--   

    ------------------------------------    FOR COMPONENTS   ----------------------------------------
    -- Tx_Rx_PHY
        signal rx_analogreset_i              : std_logic_vector(0 downto 0);
        signal rx_cal_busy_o                 : std_logic_vector(0 downto 0);                      
        signal rx_cdr_refclk0_i              : std_logic;
        signal rx_clkout_o                   : std_logic_vector(0 downto 0);                      
        signal rx_coreclkin_i                : std_logic_vector(0 downto 0);
        signal rx_digitalreset_i             : std_logic_vector(0 downto 0);
        signal rx_is_lockedtodata_o          : std_logic_vector(0 downto 0);                      
        signal rx_is_lockedtoref_o           : std_logic_vector(0 downto 0);                      
        signal rx_parallel_data_o            : std_logic_vector(39 downto 0);  
        signal rx_pma_clkslip_i              : std_logic;
        signal rx_serial_data_i              : std_logic_vector(0 downto 0); 
        signal rx_seriallpbken_i             : std_logic_vector(0 downto 0);
        signal tx_analogreset_i              : std_logic_vector(0 downto 0);
        signal tx_cal_busy_o                 : std_logic_vector(0 downto 0);                      
        signal tx_clkout_o                   : std_logic_vector(0 downto 0);                      
        signal tx_coreclkin_i                : std_logic_vector(0 downto 0);
        signal tx_digitalreset_i             : std_logic_vector(0 downto 0);
        signal tx_parallel_data_i            : std_logic_vector(39 downto 0);
        signal tx_serial_clk0_i              : std_logic_vector(0 downto 0);
        signal tx_bonding_clocks_i           : std_logic_vector(5 downto 0);
        signal tx_serial_data_o              : std_logic_vector(0 downto 0);                       
		signal unused_rx_parallel_data       : std_logic_vector(87 downto 0);
		signal unused_tx_parallel_data       : std_logic_vector(87 downto 0) := (others => '0');
    

    -- ATX_PLL
		signal pll_cal_busy_o                : std_logic;        
		signal pll_locked_o                  : std_logic;        
		signal pll_powerdown_i               : std_logic; 
		signal pll_refclk0_i                 : std_logic; 
		signal tx_serial_clk_o               : std_logic;

    -- TX_PHY_RESET
        signal tx_reset_clock_i              : std_logic;             
		signal pll_cal_busy_i                : std_logic_vector(0 downto 0); 
		signal pll_locked_i                  : std_logic_vector(0 downto 0); 
		signal pll_select_i                  : std_logic_vector(0 downto 0); 
		signal tx_reset_reset_i              : std_logic;             
		signal tx_analogreset_o              : std_logic_vector(0 downto 0);                    
		signal tx_cal_busy_i                 : std_logic_vector(0 downto 0); 
		signal tx_digitalreset_o             : std_logic_vector(0 downto 0);                    
		signal tx_ready_o                    : std_logic_vector(0 downto 0); 
        
    -- RX_PHY_RESET
        signal rx_reset_clock_i              : std_logic;             
		signal rx_reset_reset_i              : std_logic;             
		signal rx_analogreset_o              : std_logic_vector(0 downto 0);                    
		signal rx_cal_busy_i                 : std_logic_vector(0 downto 0); 
		signal rx_digitalreset_o             : std_logic_vector(0 downto 0);                    
		signal rx_is_lockedtodata_i          : std_logic_vector(0 downto 0); 
		signal rx_ready_o                    : std_logic_vector(0 downto 0);

    -- ATC_PLL_RESET
		signal atx_pll_reset_clock_i         : std_logic; 
		signal pll_powerdown_o               : std_logic_vector(0 downto 0);       
		signal atx_pll_reset_reset_i         : std_logic;  
        

	--==================================== Component Declaration ====================================--          

    component TX_RX_PHY is
        port (
            reconfig_write       : in  std_logic_vector(0 downto 0)   := (others => '0'); 
            reconfig_read        : in  std_logic_vector(0 downto 0)   := (others => '0'); 
            reconfig_address     : in  std_logic_vector(9 downto 0)   := (others => '0'); 
            reconfig_writedata   : in  std_logic_vector(31 downto 0)  := (others => '0'); 
            reconfig_readdata    : out std_logic_vector(31 downto 0);                     
            reconfig_waitrequest : out std_logic_vector(0 downto 0);                      
            reconfig_clk         : in  std_logic_vector(0 downto 0)   := (others => '0'); 
            reconfig_reset       : in  std_logic_vector(0 downto 0)   := (others => '0'); 
            rx_analogreset       : in  std_logic_vector(0 downto 0)   := (others => '0'); 
            rx_cal_busy          : out std_logic_vector(0 downto 0);                      
            rx_cdr_refclk0       : in  std_logic                      := '0';             
            rx_clkout            : out std_logic_vector(0 downto 0);                      
            rx_coreclkin         : in  std_logic_vector(0 downto 0)   := (others => '0'); 
            rx_digitalreset      : in  std_logic_vector(0 downto 0)   := (others => '0'); 
            rx_is_lockedtodata   : out std_logic_vector(0 downto 0);                      
            rx_is_lockedtoref    : out std_logic_vector(0 downto 0);                      
            rx_parallel_data     : out std_logic_vector(39 downto 0);                   
            rx_pma_clkslip       : in  std_logic_vector(0 downto 0)   := (others => '0'); 
            rx_serial_data       : in  std_logic_vector(0 downto 0)   := (others => '0'); 
            rx_seriallpbken      : in  std_logic_vector(0 downto 0)   := (others => '0'); 
            tx_analogreset       : in  std_logic_vector(0 downto 0)   := (others => '0'); 
            tx_bonding_clocks    : in  std_logic_vector(5 downto 0)   := (others => '0'); 
            tx_cal_busy          : out std_logic_vector(0 downto 0);                      
            tx_clkout            : out std_logic_vector(0 downto 0);                      
            tx_coreclkin         : in  std_logic_vector(0 downto 0)   := (others => '0'); 
            tx_digitalreset      : in  std_logic_vector(0 downto 0)   := (others => '0'); 
            tx_parallel_data     : in  std_logic_vector(39 downto 0)  := (others => '0'); 
           -- tx_serial_clk0       : in  std_logic_vector(0 downto 0)   := (others => '0'); 
            tx_serial_data       : out std_logic_vector(0 downto 0);
            unused_rx_parallel_data : out std_logic_vector(87 downto 0);                   
            unused_tx_parallel_data : in  std_logic_vector(87 downto 0) := (others => '0')            
        );
    end component TX_RX_PHY;

    component TX_ATX_PLL is
	port (
        mcgb_rst              : in std_logic;
		pll_cal_busy          : out std_logic;        
		pll_locked            : out std_logic;        
		pll_powerdown         : in  std_logic := '0'; 
		pll_refclk0           : in  std_logic := '0'; 
        reconfig_write0       : in  std_logic                     := '0';             
		reconfig_read0        : in  std_logic                     := '0';             
		reconfig_address0     : in  std_logic_vector(9 downto 0)  := (others => '0'); 
		reconfig_writedata0   : in  std_logic_vector(31 downto 0) := (others => '0'); 
		reconfig_readdata0    : out std_logic_vector(31 downto 0);                    
		reconfig_waitrequest0 : out std_logic;                                        
		reconfig_clk0         : in  std_logic                     := '0';             
		reconfig_reset0       : in  std_logic                     := '0';             
		tx_bonding_clocks     : out std_logic_vector(5 downto 0);        
		tx_serial_clk         : out std_logic         
	);
    end component TX_ATX_PLL;
    
    component Tx_PHY_RESET is
	port (
		clock           : in  std_logic                    := '0';             
		pll_cal_busy    : in  std_logic_vector(0 downto 0) := (others => '0'); 
		pll_locked      : in  std_logic_vector(0 downto 0) := (others => '0'); 
		pll_select      : in  std_logic_vector(0 downto 0) := (others => '0'); 
		reset           : in  std_logic                    := '0';             
		tx_analogreset  : out std_logic_vector(0 downto 0);                    
		tx_cal_busy     : in  std_logic_vector(0 downto 0) := (others => '0'); 
		tx_digitalreset : out std_logic_vector(0 downto 0);                    
		tx_ready        : out std_logic_vector(0 downto 0)                     
	);
    end component Tx_PHY_RESET; 

    component Rx_PHY_RESET is
	port (
		clock              : in  std_logic                    := '0';             
		reset              : in  std_logic                    := '0';             
		rx_analogreset     : out std_logic_vector(0 downto 0);                    
		rx_cal_busy        : in  std_logic_vector(0 downto 0) := (others => '0'); 
		rx_digitalreset    : out std_logic_vector(0 downto 0);                    
		rx_is_lockedtodata : in  std_logic_vector(0 downto 0) := (others => '0'); 
		rx_ready           : out std_logic_vector(0 downto 0)                     
	);
    end component Rx_PHY_RESET;
    
    component ATX_PLL_RESET is
	port (
		clock         : in  std_logic                    := '0'; 
		pll_powerdown : out std_logic_vector(0 downto 0);       
		reset         : in  std_logic                    := '0'  
	);
    end component ATX_PLL_RESET;

--=================================================================================================--
begin                 --========####   Architecture Body   ####========-- 
--=================================================================================================--
 
   --==================================== Port Mapping ==============================================--  

    TX_RX_PHY_comp:
    TX_RX_PHY 
        port map (
            reconfig_write(0)    => XCVR_PHY_I.TX_RX_PHY_reconfig_write_i,
            reconfig_read(0)     => XCVR_PHY_I.TX_RX_PHY_reconfig_read_i,
            reconfig_address     => XCVR_PHY_I.TX_RX_PHY_reconfig_address_i, 
            reconfig_writedata   => XCVR_PHY_I.TX_RX_PHY_reconfig_writedata_i,
            reconfig_readdata    => XCVR_PHY_O.TX_RX_PHY_reconfig_readdata_o,                     
            reconfig_waitrequest(0) => XCVR_PHY_O.TX_RX_PHY_reconfig_waitrequest_o,                      
            reconfig_clk(0)      => XCVR_PHY_I.TX_RX_PHY_reconfig_clk_i,
            reconfig_reset(0)    => XCVR_PHY_I.TX_RX_PHY_reconfig_reset_i,
            rx_analogreset       => rx_analogreset_i,
            rx_cal_busy          => rx_cal_busy_o,                    
            rx_cdr_refclk0       => rx_cdr_refclk0_i,             
            rx_clkout            => rx_clkout_o,                     
            rx_coreclkin         => rx_coreclkin_i,
            rx_digitalreset      => rx_digitalreset_i,
            rx_is_lockedtodata   => rx_is_lockedtodata_o,                     
            rx_is_lockedtoref    => rx_is_lockedtoref_o,                     
            rx_parallel_data     => rx_parallel_data_o,
            rx_pma_clkslip(0)    => rx_pma_clkslip_i,
            rx_serial_data       => rx_serial_data_i, 
            rx_seriallpbken      => rx_seriallpbken_i,
            tx_analogreset       => tx_analogreset_i,
            tx_bonding_clocks    => tx_bonding_clocks_i,
            tx_cal_busy          => tx_cal_busy_o,               
            tx_clkout            => tx_clkout_o,                     
            tx_coreclkin         => tx_coreclkin_i,
            tx_digitalreset      => tx_digitalreset_i,
            tx_parallel_data     => tx_parallel_data_i, 
            --tx_serial_clk0       => tx_serial_clk0_i,
            tx_serial_data       => tx_serial_data_o,
            unused_rx_parallel_data => unused_rx_parallel_data,
            unused_tx_parallel_data => unused_tx_parallel_data
        );


    TX_ATX_PLL_comp:
    TX_ATX_PLL
	port map (
        mcgb_rst                => atx_pll_reset_reset_i,
		pll_cal_busy            => pll_cal_busy_o,       
		pll_locked              => pll_locked_o,        
		pll_powerdown           => pll_powerdown_i,
		pll_refclk0             => pll_refclk0_i,
        reconfig_write0         => XCVR_PHY_I.ATX_PLL_reconfig_write_i,
		reconfig_read0          => XCVR_PHY_I.ATX_PLL_reconfig_read_i,
		reconfig_address0       => XCVR_PHY_I.ATX_PLL_reconfig_address_i,
		reconfig_writedata0     => XCVR_PHY_I.ATX_PLL_reconfig_writedata_i,
		reconfig_readdata0      => XCVR_PHY_O.ATX_PLL_reconfig_readdata_o,
		reconfig_waitrequest0   => XCVR_PHY_O.ATX_PLL_reconfig_waitrequest_o,
		reconfig_clk0           => XCVR_PHY_I.ATX_PLL_reconfig_clk_i,
		reconfig_reset0         => XCVR_PHY_I.ATX_PLL_reconfig_reset_i,
        tx_bonding_clocks    => tx_bonding_clocks_i,
		tx_serial_clk           => tx_serial_clk_o
	);
    
    Tx_PHY_RESET_comp:
    Tx_PHY_RESET
    port map(
        clock              => tx_reset_clock_i,
        pll_cal_busy       => pll_cal_busy_i,
		pll_locked         => pll_locked_i,
		pll_select         => pll_select_i,
		reset              => tx_reset_reset_i,
		tx_analogreset     => tx_analogreset_o,
		tx_cal_busy        => tx_cal_busy_i,
		tx_digitalreset    => tx_digitalreset_o,
		tx_ready           => tx_ready_o
    );

    Rx_PHY_RESET_comp:
    Rx_PHY_RESET
    port map(
        clock              => rx_reset_clock_i,
		reset              => rx_reset_reset_i,
		rx_analogreset     => rx_analogreset_o,
		rx_cal_busy        => rx_cal_busy_i,
		rx_digitalreset    => rx_digitalreset_o,
		rx_is_lockedtodata => rx_is_lockedtodata_i,
		rx_ready           => rx_ready_o
    );

    ATX_PLL_RESET_comp:
    ATX_PLL_RESET
    port map(
        clock              => atx_pll_reset_clock_i,
		pll_powerdown      => pll_powerdown_o,
		reset              => atx_pll_reset_reset_i
    );

   --==============================================================================================--
   --=================================== CLOCK CONNECTION =========================================--
   --==============================================================================================--

    -- Tx_PHY
		                   
		tx_coreclkin_i                <= tx_clkout_o; 
		tx_serial_clk0_i(0)           <= tx_serial_clk_o;
        TX_PMA_WORD_CLK_O             <= tx_clkout_o(0);
		

    -- RX_PHY    
		rx_cdr_refclk0_i              <= XCVR_REF_CLK_I;            
		rx_coreclkin_i                <= rx_clkout_o; 
        RX_PMA_WORD_CLK_O             <= rx_clkout_o(0);
		
    -- ATX_PLL
		pll_refclk0_i                 <= XCVR_REF_CLK_I; 

    -- TX_PHY_RESET
        tx_reset_clock_i              <= USER_LOGIC_CLK_I;             
        
    -- RX_PHY_RESET
        rx_reset_clock_i              <= USER_LOGIC_CLK_I;          
		
    -- ATC_PLL_RESET
		atx_pll_reset_clock_i         <= USER_LOGIC_CLK_I;
		

   --==============================================================================================--
   --=================================== RESET CONNECTION =========================================--
   --==============================================================================================--
     -- Tx_PHY
		tx_analogreset_i              <= tx_analogreset_o; 
		tx_digitalreset_i             <= tx_digitalreset_o; 

    -- RX_PHY    
		rx_analogreset_i              <= rx_analogreset_o;
		rx_digitalreset_i             <= rx_digitalreset_o; 

    -- ATX_PLL
		pll_powerdown_i               <= pll_powerdown_o(0); 

    -- TX_PHY_RESET
		tx_reset_reset_i              <= GENERAL_RESET_I or TX_PHY_RESET_I;             
        
    -- RX_PHY_RESET
		rx_reset_reset_i              <= GENERAL_RESET_I or RX_PHY_RESET_I;             

    -- ATC_PLL_RESET
		atx_pll_reset_reset_i         <= GENERAL_RESET_I or ATX_PLL_RESET_I; 


   --==============================================================================================--
   --=================================== READY CONNECTION =========================================--
   --==============================================================================================--

    -- Tx_PHY
		XCVR_PHY_O.tx_cal_busy_o                 <= tx_cal_busy_o(0);                 
		
    -- RX_PHY    
		XCVR_PHY_O.rx_cal_busy_o                 <= rx_cal_busy_o(0);                   
		XCVR_PHY_O.rx_is_lockedtodata_o          <= rx_is_lockedtodata_o(0);                   
		XCVR_PHY_O.rx_is_lockedtoref_o           <= rx_is_lockedtoref_o(0);                   
		
    -- ATX_PLL
		XCVR_PHY_O.pll_cal_busy_o                <= pll_cal_busy_o;       
		XCVR_PHY_O.pll_locked_o                  <= pll_locked_o;       
		
    -- TX_PHY_RESET
        pll_cal_busy_i(0)                         <= pll_cal_busy_o or tx_cal_busy_o(0);
		pll_locked_i(0)                           <= pll_locked_o;
		pll_select_i                              <= "0";
		tx_cal_busy_i                             <= tx_cal_busy_o;
		XCVR_PHY_O.tx_ready_o                     <= tx_ready_o(0);
        
    -- RX_PHY_RESET
		rx_cal_busy_i                             <= rx_cal_busy_o;
		rx_is_lockedtodata_i                      <= rx_is_lockedtodata_o;
		XCVR_PHY_O.rx_ready_o                     <= rx_ready_o(0);

     -- ATC_PLL_RESET
       
        
   --==============================================================================================--
   --=================================== CONTROL CONNECTION =======================================--
   --==============================================================================================--
    -- Tx_PHY
    -- RX_PHY    
		rx_seriallpbken_i(0)             <= XCVR_PHY_I.rx_seriallpbken_i;
        rx_pma_clkslip_i                 <= XCVR_PHY_I.rx_pma_clkslip_i;
    -- ATX_PLL

    -- TX_PHY_RESET
        
    -- RX_PHY_RESET

    -- ATC_PLL_RESET
             
   --==============================================================================================--
   --=================================== DATA LINK CONNECTION =====================================--
   --==============================================================================================--   
    
    -- Tx_PHY
		tx_parallel_data_i                      <= XCVR_PHY_I.tx_parallel_data_i;
		XCVR_PHY_O.tx_serial_data_o             <= tx_serial_data_o(0);

    -- RX_PHY    
		XCVR_PHY_O.rx_parallel_data_o           <= rx_parallel_data_o;
		rx_serial_data_i(0)                     <= XCVR_PHY_I.rx_serial_data_i;


end architecture mixed;