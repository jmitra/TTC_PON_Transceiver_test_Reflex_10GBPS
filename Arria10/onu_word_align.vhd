--==============================================================================
--! @file onu_word_align.vhd
--==============================================================================
--! Standard library
library ieee;
--! Standard packages
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--! Specific packages

--------------------------------------------------------------------------------
-- --
-- CERN, PH-ESE, PON demo
-- --
--------------------------------------------------------------------------------
--
-- unit name: ONU word aligner (onu_word_align)
--
--! @brief This module performs word alignment in the ONU receiver path.
--! The word alignment module controls the barrel shifter, in order to align
--! the incoming data. The word alignment is successful, if the comma character
--! (K28.5) is found in the upper 10 bits of the received 20-bit data word.
--
--! @author Csaba Soos (csaba.soos AT cern.ch)
--
--! @date 13-07-2009
--
--! @version 1.0
--
--! @details
--!
--! <b>Dependencies:</b>\n
--! 
--!
--! <b>References:</b>\n
--!
--! <b>Modified by:</b>\n
--! Author: Csaba Soos
--------------------------------------------------------------------------------
--! \n\n<b>Last changes:</b>\n
--! 13-07-2009 CS  Created\n
--------------------------------------------------------------------------------
--! @todo Make it more robust\n
--! TODO details
--------------------------------------------------------------------------------

--==============================================================================
--! Entity declaration for ONU word aligner
--==============================================================================
entity onu_word_align is

    generic (
        g_COMMAMASK     : std_logic_vector := "0001111111";
        g_PCOMMAPATTERN : std_logic_vector := "0101111100";
        g_NCOMMAPATTERN : std_logic_vector := "1010000011");
    port (
        clock_i        : in  std_logic;
        reset_i        : in  std_logic;
        enable_i       : in  std_logic;
        rxdata_i       : in  std_logic_vector(39 downto 0);
        nshift_o       : out std_logic_vector(5 downto 0);
        aligned_o      : out std_logic;
        reset_gtx_o    : out std_logic;
        rx_slide_o     : out std_logic;
        rx8b10ben_o    : out std_logic;
--                      word_align_FSM_o        : out std_logic_vector(2 downto 0);
        comma_detect_o : out std_logic;
        rx_k28_5_o     : out std_logic;
        rx_k28_1_o     : out std_logic
        );

end entity onu_word_align;

--==============================================================================
-- architecture declaration
--==============================================================================
architecture rtl of onu_word_align is

    attribute MARK_DEBUG : string;

    type t_alignment_state is (
        RESET,
        SEARCH,
        ROTATE,
        WAITING,
        RESET_GTX,
        GTX_BITSLIDE,
        GTX_BITSLIDE_WAIT,
        GTX_BITSLIDE_TEST,
        ALIGNED);
    signal s_align_FSM_state : t_alignment_state;

    signal s_comma_detect   : std_logic;
    signal s_frame_aligned  : std_logic;
    signal s_sliding_done   : std_logic;
    signal s_search_timeout : std_logic;
    signal rotate_count     : unsigned(5 downto 0);
    signal reset_gtx_delay  : integer range 0 to 15;
    signal gtx_slide_count  : unsigned(5 downto 0);
    signal gtx_slide_delay  : integer range 0 to 63;
    
    attribute MARK_DEBUG of s_align_FSM_state, s_comma_detect : signal is "TRUE";
    
--==============================================================================
-- architecture begin
--==============================================================================
begin  -- architecture rtl
    comma_detect_o <= s_comma_detect;

--       word_align_FSM_o <= "000" when s_align_FSM_state = RESET        else --##############
--                                                              "001" when s_align_FSM_state = SEARCH    else --##############
--                                                              "010" when s_align_FSM_state = ROTATE  else --##############
--                                                              "011" when s_align_FSM_state = WAITING   else --##############
--                                                              "100" when s_align_FSM_state = ALIGNED   else --##############
--                                                              "111";  

    ----------------------------------------------------------------------------
    --! Process Alignment FSM (state transitions)
    --! read:  clock_i, reset_i, s_frame_aligned, s_search_timeout\n
    --!        s_sliding_done\n
    --! write: \n
    --! r/w:   s_align_FSM_state\n
    ----------------------------------------------------------------------------
    p_align_FSM_transitions : process (clock_i) is
    begin  -- process p_align_FSM_transitions
        if clock_i'event and clock_i = '1' then  -- rising clock edge
            if reset_i = '1' then
                s_align_FSM_state <= RESET;
                reset_gtx_delay <= 0;
                gtx_slide_delay <= 0;
                gtx_slide_count <= (others => '0');
                reset_gtx_o <= '0';
                rx_slide_o <= '0';
            else
                reset_gtx_o <= '0';
                rx_slide_o <= '0';
                s_align_FSM_state <= s_align_FSM_state;
                case s_align_FSM_state is

                    when RESET =>
                        if enable_i = '1' then
                            s_align_FSM_state <= SEARCH;
                        end if;

                    when SEARCH =>
                        reset_gtx_delay <= 0;
                        gtx_slide_count <= 40-rotate_count;
                        if s_frame_aligned = '1' then
                            if rotate_count = 0 then
                                s_align_FSM_state <= ALIGNED;
                            elsif rotate_count(0) = '1' then
                                s_align_FSM_state <= RESET_GTX;
                            else
                                s_align_FSM_state <= GTX_BITSLIDE;
                            end if;
                        elsif s_search_timeout = '1' then
                            s_align_FSM_state <= ROTATE;
                        end if;

                    when ROTATE =>
                        s_align_FSM_state <= WAITING;

                    when WAITING =>
                        if s_sliding_done = '1' then
                            s_align_FSM_state <= SEARCH;
                        end if;

                    when RESET_GTX =>
                        reset_gtx_o <= '1';
                        reset_gtx_delay <= reset_gtx_delay + 1;
                        if reset_gtx_delay = 8 then
                            s_align_FSM_state <= RESET;
                        else
                            s_align_FSM_state <= RESET_GTX;
                        end if;

                    when GTX_BITSLIDE =>
                        rx_slide_o <= '1';
                        gtx_slide_delay <= 0;
                        gtx_slide_count <= gtx_slide_count - 1;
                        s_align_FSM_state <= GTX_BITSLIDE_WAIT;

                    when GTX_BITSLIDE_WAIT => 
                        gtx_slide_delay <= gtx_slide_delay + 1;
                        if gtx_slide_delay = 32 then
                            s_align_FSM_state <= GTX_BITSLIDE_TEST;
                        else
                            s_align_FSM_state <= GTX_BITSLIDE_WAIT;
                        end if;

                    when GTX_BITSLIDE_TEST =>
                        if gtx_slide_count > 0 then
                            s_align_FSM_state <= GTX_BITSLIDE;
                        else
                            s_align_FSM_state <= RESET;
                        end if;

                    when ALIGNED =>
                        if enable_i = '1' and s_frame_aligned = '0' then
                            s_align_FSM_state <= RESET;
                        end if;

                    when others =>
                        s_align_FSM_state <= RESET;

                end case;
            end if;
        end if;
    end process p_align_FSM_transitions;

    rx8b10ben_o <= '1' when s_align_FSM_state = ALIGNED else '0';
    aligned_o <= '1' when s_align_FSM_state = ALIGNED else '0';
    
    ----------------------------------------------------------------------------
    --! Process Rotate control
    --! read:  clock_i, reset_i, s_align_FSM_state\n
    --! write: nshift_o\n
    --! r/w:   \n
    ----------------------------------------------------------------------------
    p_rotate_control : process (clock_i, reset_i) is
    begin  -- process p_rotate_control
        if reset_i = '1' then         -- asynchronous reset (active low)
            nshift_o        <= (others => '0');
            rotate_count <= (others => '0');
        elsif clock_i'event and clock_i = '1' then  -- rising clock edge
            if s_align_FSM_state = ROTATE then
                if rotate_count < 39 then
                    rotate_count <= rotate_count + 1;
                else
                    rotate_count <= (others => '0');
                end if;
            end if;
            nshift_o <= std_logic_vector(rotate_count);
        end if;
    end process p_rotate_control;

    ----------------------------------------------------------------------------
    --! Process Comma detect
    --! read:  clock_i, reset_i, rxdata_i\n
    --! write: s_comma_detect\n
    --! r/w:   \n
    ----------------------------------------------------------------------------
    p_comma_detect : process (clock_i, reset_i) is
    begin  -- process p_comma_detect
        if reset_i = '1' then         -- asynchronous reset (active high)
            s_comma_detect <= '0';
            rx_k28_5_o <= '0';
            rx_k28_1_o <= '0';
        elsif clock_i'event and clock_i = '1' then  -- rising clock edge
            if s_align_FSM_state = ALIGNED then
                s_comma_detect <= '0';
                if (rxdata_i(8) = '1' and rxdata_i(7 downto 0) = x"bc") or
                   (rxdata_i(8) = '1' and rxdata_i(7 downto 0) = x"3c") then
                   s_comma_detect <= '1';
                end if;
                rx_k28_5_o <= '0';
                if rxdata_i(8) = '1' and rxdata_i(7 downto 0) = x"bc" then
                    rx_k28_5_o <= '1';
                end if; 
                rx_k28_1_o <= '0';
                if rxdata_i(8) = '1' and rxdata_i(7 downto 0) = x"3c" then
                    rx_k28_1_o <= '1';
                end if; 
            else
                rx_k28_5_o <= '0';
                rx_k28_1_o <= '0';
                if (rxdata_i(9 downto 0) and g_COMMAMASK) = (g_PCOMMAPATTERN and g_COMMAMASK) or
                   (rxdata_i(9 downto 0) and g_COMMAMASK) = (g_NCOMMAPATTERN and g_COMMAMASK) then
                    s_comma_detect <= '1';
                else
                    s_comma_detect <= '0';
                end if;
            end if;
        end if;
    end process p_comma_detect;

    ----------------------------------------------------------------------------
    --! Process Frame check
    --! read:  clock_i, reset_i, s_comma_detect\n
    --! write: s_frame_aligned\n
    --! r/w:   \n
    ----------------------------------------------------------------------------
    p_frame_check : process (clock_i, reset_i) is
        variable v_word_counter       : unsigned(8 downto 0);
        variable v_good_frame_counter : unsigned(2 downto 0);
    begin  -- process p_frame_check
        if reset_i = '1' then         -- asynchronous reset (active high)
            s_frame_aligned      <= '0';
            v_word_counter       := "000000100";    --010000000
            v_good_frame_counter := "000";
        elsif clock_i'event and clock_i = '1' then  -- rising clock edge
            
            if v_good_frame_counter < 2 or s_align_FSM_state = RESET then
                s_frame_aligned <= '0';
            elsif v_good_frame_counter > 4 then
                s_frame_aligned <= '1';
            else
                s_frame_aligned <= s_frame_aligned;
            end if;

            if s_align_FSM_state = RESET then
                v_good_frame_counter := "000";
            else
                if v_word_counter(8) = '1' then
                    if s_comma_detect = '1' and v_good_frame_counter < 7 then
                        v_good_frame_counter := v_good_frame_counter + 1;
                    elsif s_comma_detect = '0' and v_good_frame_counter > 0 then
                        v_good_frame_counter := v_good_frame_counter - 1;
                    end if;
                elsif s_comma_detect = '1' then
                    if v_good_frame_counter > 0 then
                        v_good_frame_counter := v_good_frame_counter - 1;
                    end if;
                end if;
            end if;

            if s_comma_detect = '1' or v_word_counter(8) = '1' then
                v_word_counter := "000000100";  --010000000
            else
                v_word_counter := v_word_counter - 1;
            end if;
        end if;
    end process p_frame_check;

    ----------------------------------------------------------------------------
    --! Process Search timer
    --! read:  clock_i, reset_i, s_align_FSM_state\n
    --! write: s_search_timeout\n
    --! r/w:   \n
    ----------------------------------------------------------------------------
    p_search_timer : process (clock_i, reset_i) is
        variable v_search_time_counter : unsigned(10 downto 0);
    begin  -- process p_search_timer
        if reset_i = '1' then         -- asynchronous reset (active high)
            s_search_timeout      <= '0';
            v_search_time_counter := "01111111111";
        elsif clock_i'event and clock_i = '1' then  -- rising clock edge
            s_search_timeout <= v_search_time_counter(10);
            if s_align_FSM_state = SEARCH then
                v_search_time_counter := v_search_time_counter - 1;
            else
                v_search_time_counter := "01111111111";
            end if;
        end if;
    end process p_search_timer;

    ----------------------------------------------------------------------------
    --! Process Wait timer
    --! read:  clock_i, reset_i, s_align_FSM_state\n
    --! write: s_sliding_done\n
    --! r/w:   \n
    ----------------------------------------------------------------------------
--    p_wait_timer: process (clock_i, reset_i) is
--        variable v_wait_time_counter : unsigned(2 downto 0);
--    begin  -- process p_wait_timer
--        if reset_i = '1' then         -- asynchronous reset (active high)
--            s_sliding_done <= '0';
--            v_wait_time_counter := "011";
--        elsif clock_i'event and clock_i = '1' then  -- rising clock edge
--            s_sliding_done <= v_wait_time_counter(2);
--            if s_align_FSM_state = WAITING then
--                v_wait_time_counter := v_wait_time_counter - 1;
--            else
--                v_wait_time_counter := "011";
--            end if;
--        end if;
--    end process p_wait_timer;



    ----------------------------------------------------------------------------
    --! Process Wait timer
    --! read:  clock_i, reset_i, s_align_FSM_state\n
    --! write: s_sliding_done\n
    --! r/w:   \n
    ----------------------------------------------------------------------------
    p_wait_timer : process (clock_i, reset_i) is
        variable v_wait_time_counter : unsigned(3 downto 0);
        constant delay               : unsigned(3 downto 0) := x"4";
    begin  -- process p_wait_timer
        if reset_i = '1' then         -- asynchronous reset (active high)
            s_sliding_done      <= '0';
            v_wait_time_counter := x"0";
        elsif clock_i'event and clock_i = '1' then  -- rising clock edge

            if s_align_FSM_state = SEARCH then
                s_sliding_done      <= '0';
                v_wait_time_counter := delay;  -- load down counter
                
            elsif s_align_FSM_state = WAITING then
                if v_wait_time_counter /= x"0" then
                    v_wait_time_counter := v_wait_time_counter-1;  -- count down when not zero
                else
                    s_sliding_done <= '1';
                end if;
            end if;
        end if;
    end process;





end architecture rtl;
--==============================================================================
-- architecture end
--==============================================================================
