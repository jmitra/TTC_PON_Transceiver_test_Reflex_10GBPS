-------------------------------------------------------------------------------
-- Title      : Phase Calculation Using XOR Phase Detection Logic
-- Project    : PHASE CALLIBRATION
-------------------------------------------------------------------------------
-- File       : phase_calc.vhd
-- Author     : Jubin MITRA
-- Company    : 
-- Created    : 04-12-2015
-- Last update: 04-12-2015
-- Platform   : 
-- Standard   : VHDL'93/08
-------------------------------------------------------------------------------
-- Description: 
-- input  clock
-- input phase shifted clock
-- sample clock			
-- output integer part 		
-- output fractional part
-- output decimal part			
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 04-12-2015  1.0      Jubin	Created
-------------------------------------------------------------------------------



--=================================================================================================--
--#################################################################################################--
--=================================================================================================--


-- IEEE VHDL standard library:
library ieee;
use ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all; 
USE ieee.STD_LOGIC_UNSIGNED.all; 
use ieee.numeric_std.all;

--=================================================================================================--
--#######################################   Entity   ##############################################--
--=================================================================================================--

entity phase_calc is
   generic (
		constant NUMBER_OF_CYCLES			    : std_logic_vector( 31 downto 0) := X"0000_FFFF";
		-- ======================================================================================== --
		-- For calibration in degrees put 360 deg or time period T in ps unit like 8000 ps for 120 MHz
		-- ======================================================================================== --
		constant CALIBRATION_FACTOR				: integer						 := 4000
   );
   port (
  
      --================--
      -- Reset & Clocks --
      --================--    
      
      -- Reset:
      ---------
      
      RESET_I                                   : in  std_logic;
  
      -- Clocks:
      ----------
      
      REF_CLK_I                                 : in  std_logic; --  120 MHz
      PHASE_SHIFTED_CLK_I                       : in  std_logic; --  120 MHz + \theta
	  SAMPLE_CLK_I								: in  std_logic;
      
      --==============--
      -- PHASE VALUES --
      --==============--
      
      PHASE_VALUE_INTEGER_PART_O                : out std_logic_vector( 31 downto 0);
      PHASE_VALUE_FRACTIONAL_PART_O             : out std_logic_vector( 31 downto 0);
	  PHASE_VALUE_SIGN_O						: out std_logic;

	  PHASE_CALC_DONE_O							: out std_logic
   
   );
end phase_calc;

--=================================================================================================--
--####################################   Architecture   ###########################################-- 
--=================================================================================================--

architecture behavioral of phase_calc is
	
	component delay_insert_pll is
		port (
			rst      : in  std_logic := 'X';
			refclk   : in  std_logic := 'X';
			outclk_0 : out std_logic        
		);
	end component delay_insert_pll;

   --==================================== Signal Definition =====================================--   
		-- PHASE EVALUATE
		signal phase_count  					: std_logic_vector( 31 downto 0) := (others => '0');
		signal period_count 					: std_logic_vector( 31 downto 0) := (others => '0');
		signal step_count						: std_logic_vector( 31 downto 0) := (others => '0');
		signal phase_eval_done					: std_logic;
        signal REF_CLK_I_sample                 : std_logic:='0';
        signal PHASE_SHIFTED_CLK_I_sample       : std_logic:='0';

        signal PHASE_SHIFTED_CLK_I_delay        : std_logic:='0';
		
		-- PHASE COMPUTE
		signal alpha							: std_logic_vector( 31 downto 0) := (others => '0');
		signal beta								: std_logic_vector( 31 downto 0) := (others => '0');
		signal delta							: std_logic						 :=  		   '0' ;
		signal phi								: std_logic_vector( 63 downto 0) := (others => '0');
		signal phase_value						: std_logic_vector( 63 downto 0) := (others => '0');
		
		
		signal K								: std_logic_vector( 31 downto 0) := std_logic_vector(to_unsigned(CALIBRATION_FACTOR, 32));  
		signal step_counter						: integer						 := 0;
		signal phase_calc_done					: std_logic;

		--=====================================================================================--  

--=================================================================================================--
begin                 --========####   Architecture Body   ####========-- 
--=================================================================================================--  
   	u0 : component delay_insert_pll
		port map (
			rst      => RESET_I,      
			refclk   => PHASE_SHIFTED_CLK_I,  
			outclk_0 => PHASE_SHIFTED_CLK_I_delay  
		);
   --==================================== User Logic =============================================--   
	   
	   
	   clock_sampling_p:
	   process(RESET_I,SAMPLE_CLK_I)
	   begin
			if RESET_I = '1' then
				phase_count 	<= 	(others => '0');
				period_count	<=	(others => '0');
				step_count		<= 	(others => '0');
				
				phase_eval_done <=  '0';
			elsif rising_edge(SAMPLE_CLK_I) then
            
                REF_CLK_I_sample            <= REF_CLK_I;
                PHASE_SHIFTED_CLK_I_sample  <= PHASE_SHIFTED_CLK_I;
                
				if step_count <= NUMBER_OF_CYCLES then
					if (  (REF_CLK_I_sample ='1' and PHASE_SHIFTED_CLK_I_sample = '0') or (REF_CLK_I_sample ='0' and PHASE_SHIFTED_CLK_I_sample = '1') ) then
						phase_count 	<= phase_count   + '1';
					end if;
					
					if ( REF_CLK_I_sample	= 	'1' ) then
						period_count    <= period_count   + '1';
					end if;
                    
					step_count		 <= step_count   + '1';
				else
					phase_eval_done <=  '1';
				end if;
								
			end if;
	   end process;
	
   --============================================================================================--
   --================================# Phase Value Computation #=================================--
   --============================================================================================--
   
   phase_compute:
   process(RESET_I,REF_CLK_I)
   begin
		if RESET_I = '1' then
			alpha							<= (others => '0');
			beta							<= (others => '0');
			delta							<=            '0' ;
			phi (31 downto 0 )				<= (others => '0'); 
			phi	(63 downto 32)				<= K;
			phase_value						<= (others => '0'); 
			step_counter					<= 0;
			
			phase_calc_done					<= '0';
			
		elsif rising_edge(REF_CLK_I) then
		
			if phase_calc_done = '0' and phase_eval_done = '1' then
				if step_counter = 0 then
                        alpha						<=	phase_count;
                        beta 						<=  period_count;                    
					step_counter				<=  step_counter + 1;
					phi							<=  "00"& K & "00" & x"000_0000";
					phase_value					<= (others => '0');
				else
					beta						<= '0' & beta(31 downto 1);
					phi 						<= '0' &  phi(63 downto 1);
					
					if  alpha >= beta then
						alpha 					<= alpha - beta;
						phase_value				<= phase_value + phi;
					end if;
					
					if beta = x"0000_0000" then
						phase_calc_done 		<= '1';
                            phase_value         <= phase_value;
					end if;	
					
				end if;
			end if;
		
		end if;
   end process;
   
   --===========================================================================================--
   --================================# Phase Sign Computation #=================================--
   --===========================================================================================--
   
   phase_sign:
   process(RESET_I,REF_CLK_I)
   begin
		if RESET_I = '1' then
			PHASE_VALUE_SIGN_O <= '0';
		elsif rising_edge(REF_CLK_I) then
			if phase_eval_done = '1' then
				PHASE_VALUE_SIGN_O	<= PHASE_SHIFTED_CLK_I;
			end if;				
		end if;
   end process;
   
   --===========================================================================================--
   --================================# Phase Value Output Map #=================================--
   --===========================================================================================--
   
   PHASE_VALUE_INTEGER_PART_O					<= phase_value (63 downto 32);
   PHASE_VALUE_FRACTIONAL_PART_O				<= phase_value (31 downto  0);
   PHASE_CALC_DONE_O							<= phase_calc_done;
   
end behavioral;
--=================================================================================================--
--#################################################################################################--
--=================================================================================================--
--=================================================================================================--