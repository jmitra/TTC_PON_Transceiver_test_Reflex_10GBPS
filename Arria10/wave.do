onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/RESET
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/REF_CLK
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/tx_serial_data
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/rx_serial_data
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PCS_LAYER_comp/GENERAL_RESET_I
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PCS_LAYER_comp/prbs_type
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PCS_LAYER_comp/TX_PCS_RESET_I
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PCS_LAYER_comp/RX_PCS_RESET_I
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TX_PHY_RESET_I
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/RX_PHY_RESET_I
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PCS_LAYER_comp/TX_PMA_WORD_CLK_I
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PCS_LAYER_comp/RX_PMA_WORD_CLK_I
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/tx_serial_clk_o
add wave -noupdate -divider TX_RX_PHY
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/reconfig_clk
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/reconfig_reset
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/rx_analogreset
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/rx_cal_busy
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/rx_cdr_refclk0
add wave -noupdate -radix hexadecimal -childformat {{/ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/rx_clkout(0) -radix hexadecimal}} -subitemconfig {/ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/rx_clkout(0) {-height 18 -radix hexadecimal}} /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/rx_clkout
add wave -noupdate -radix hexadecimal -childformat {{/ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/rx_coreclkin(0) -radix hexadecimal}} -subitemconfig {/ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/rx_coreclkin(0) {-height 18 -radix hexadecimal}} /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/rx_coreclkin
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/rx_digitalreset
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/rx_is_lockedtodata
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/rx_is_lockedtoref
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/rx_parallel_data
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/rx_pma_clkslip
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/rx_serial_data
add wave -noupdate -radix hexadecimal -childformat {{/ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/rx_seriallpbken(0) -radix hexadecimal}} -subitemconfig {/ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/rx_seriallpbken(0) {-height 18 -radix hexadecimal}} /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/rx_seriallpbken
add wave -noupdate -radix hexadecimal -childformat {{/ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/tx_analogreset(0) -radix hexadecimal}} -subitemconfig {/ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/tx_analogreset(0) {-height 18 -radix hexadecimal}} /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/tx_analogreset
add wave -noupdate -radix hexadecimal -childformat {{/ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/tx_bonding_clocks(5) -radix hexadecimal} {/ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/tx_bonding_clocks(4) -radix hexadecimal} {/ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/tx_bonding_clocks(3) -radix hexadecimal} {/ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/tx_bonding_clocks(2) -radix hexadecimal} {/ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/tx_bonding_clocks(1) -radix hexadecimal} {/ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/tx_bonding_clocks(0) -radix hexadecimal}} -subitemconfig {/ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/tx_bonding_clocks(5) {-height 18 -radix hexadecimal} /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/tx_bonding_clocks(4) {-height 18 -radix hexadecimal} /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/tx_bonding_clocks(3) {-height 18 -radix hexadecimal} /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/tx_bonding_clocks(2) {-height 18 -radix hexadecimal} /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/tx_bonding_clocks(1) {-height 18 -radix hexadecimal} /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/tx_bonding_clocks(0) {-height 18 -radix hexadecimal}} /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/tx_bonding_clocks
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/tx_cal_busy
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/tx_clkout
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/tx_coreclkin
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/tx_digitalreset
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/tx_parallel_data
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/tx_serial_data
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/unused_rx_parallel_data
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/unused_tx_parallel_data
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_RX_PHY_comp/xcvr_native_a10_0_rx_parallel_data
add wave -noupdate -divider TX_ATX_PLL
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_ATX_PLL_comp/mcgb_rst
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_ATX_PLL_comp/pll_cal_busy
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_ATX_PLL_comp/pll_locked
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_ATX_PLL_comp/pll_powerdown
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_ATX_PLL_comp/pll_refclk0
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_ATX_PLL_comp/reconfig_write0
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_ATX_PLL_comp/reconfig_read0
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_ATX_PLL_comp/reconfig_address0
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_ATX_PLL_comp/reconfig_writedata0
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_ATX_PLL_comp/reconfig_readdata0
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_ATX_PLL_comp/reconfig_waitrequest0
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_ATX_PLL_comp/reconfig_clk0
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_ATX_PLL_comp/reconfig_reset0
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_ATX_PLL_comp/tx_bonding_clocks
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/TX_ATX_PLL_comp/tx_serial_clk
add wave -noupdate -divider TX_PHY_RESET
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/Tx_PHY_RESET_comp/clock
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/Tx_PHY_RESET_comp/pll_cal_busy
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/Tx_PHY_RESET_comp/pll_locked
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/Tx_PHY_RESET_comp/pll_select
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/Tx_PHY_RESET_comp/reset
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/Tx_PHY_RESET_comp/tx_analogreset
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/Tx_PHY_RESET_comp/tx_cal_busy
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/Tx_PHY_RESET_comp/tx_digitalreset
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/Tx_PHY_RESET_comp/tx_ready
add wave -noupdate -divider RX_PHY_RESET
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/Rx_PHY_RESET_comp/clock
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/Rx_PHY_RESET_comp/reset
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/Rx_PHY_RESET_comp/rx_analogreset
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/Rx_PHY_RESET_comp/rx_cal_busy
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/Rx_PHY_RESET_comp/rx_digitalreset
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/Rx_PHY_RESET_comp/rx_is_lockedtodata
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/Rx_PHY_RESET_comp/rx_ready
add wave -noupdate -divider ATX_PLL_RESET
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/ATX_PLL_RESET_comp/clock
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/ATX_PLL_RESET_comp/pll_powerdown
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/TTC_PHY_LAYER_comp/ATX_PLL_RESET_comp/reset
add wave -noupdate -divider {PRBS DATA GEN}
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/prbs_data_gen_comp/clock_i
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/prbs_data_gen_comp/mode_i
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/prbs_data_gen_comp/ena_i
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/prbs_data_gen_comp/sel_i
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/prbs_data_gen_comp/seed_i
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/prbs_data_gen_comp/dout_o
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/prbs_data_gen_comp/prbs7_seed
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/prbs_data_gen_comp/prbs7_bits
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/prbs_data_gen_comp/prbs7_dout
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/prbs_data_gen_comp/prbs23_seed
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/prbs_data_gen_comp/prbs23_bits
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/prbs_data_gen_comp/prbs23_dout
add wave -noupdate -divider {PRBS DATA CHECKER}
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/prbs_data_check_comp/clock_i
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/prbs_data_check_comp/reset_i
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/prbs_data_check_comp/ena_i
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/prbs_data_check_comp/sel_i
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/prbs_data_check_comp/din_i
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/prbs_data_check_comp/lock_o
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/prbs_data_check_comp/error_o
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/prbs_data_check_comp/nerrs_o
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/prbs_data_check_comp/refgen_o
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/prbs_data_check_comp/ZEROS
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/prbs_data_check_comp/refgen_mode
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/prbs_data_check_comp/refgen_ena
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/prbs_data_check_comp/refgen_sel
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/prbs_data_check_comp/refgen_seed
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/prbs_data_check_comp/refgen_dout
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/prbs_data_check_comp/datain
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/prbs_data_check_comp/datain_r
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/prbs_data_check_comp/ena_r
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/prbs_data_check_comp/ena_rr
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/prbs_data_check_comp/data_error
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/prbs_data_check_comp/search_mode
add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/prbs_data_check_comp/errd_bits
add wave -noupdate -divider {State Machine For LOCK}

add wave -noupdate -radix hexadecimal /ttc_pon_top_tb/TTC_PON_TOP_comp/rx_bitslip_fsm

TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {40785962 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 372
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {38857126 ps} {77626731 ps}

run -all