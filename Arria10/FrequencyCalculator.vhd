-------------------------------------------------------------------------------
-- Title      : Frequency Calculator
-- Project    : Calculation of unknown frequency with respect to known frequency 
-------------------------------------------------------------------------------
-- File       : FrequencyCalculator.vhd
-- Author     : Jubin MITRA
-- Contact	  : jubin.mitra@cern.ch
-- Company    : VECC, Kolkata, India
-- Created    : 15-02-2016
-- Last update: 15-02-2016
-- Platform   : 
-- Standard   : VHDL'93/08
-------------------------------------------------------------------------------
-- Description: 
-- General : It has an accuracy of 6 digit after decimal (Value Format : XXX.XXXXXX MHz or XXX,XXX,XXX Hz)
-- GENERIC : Reference Frequency Value
-- INPUT   : Reference Clock
--			 Unknown   Clock
-- OUTPUT  : Calculated Frequency Value								
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 15-02-2016  1.0      Jubin	Created
-- 21-05-2016  2.0      Jubin   Added Resolution upto 1 Hz
-- 30-05-2016  2.1      Jubin   Auto Reset
-------------------------------------------------------------------------------



--=================================================================================================--
--#################################################################################################--
--=================================================================================================--


library ieee;
use ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all; 
USE ieee.STD_LOGIC_UNSIGNED.all; 
use ieee.numeric_std.all;
use IEEE.math_real.all;

--=================================================================================================--
--#######################################   Entity   ##############################################--
--=================================================================================================--
entity FrequencyCalculator is
   generic (
		constant REFERENCE_FREQUENCY_VALUE    			: integer:= 100
   );
   port (
  
      --================--
      -- Reset & Clocks --
      --================--    
      
      -- Reset:
      ---------
      
      RESET_I                                    : in  std_logic;
  
      -- REFERENCE Clock:
      ---------------
      
      CLK_REF_I   		                         : in  std_logic; 
      
	  -- UNKNOWN Clock:
      ---------------
      
      CLK_UNKNOWN_I   		                     : in  std_logic; 
	  
      -- Output Frequency Value:
      ------------------------
      
      CLK_FREQUENCY_VALUE_O						 : out std_logic_vector(31 downto 0);
      COMPUTATION_DONE_O                         : out std_logic
   
   );
end entity;

--=================================================================================================--
--####################################   Architecture   ###########################################-- 
--=================================================================================================--
architecture behavioral of FrequencyCalculator is

	--==================================== Constant Definition ====================================--   
	

	--==================================== Signal Definition ======================================--   
        signal s_REFERENCE_FREQUENCY_VALUE  		: std_logic_vector(31 downto 0):=(others=>'0');
        signal ref_clk_counter						: std_logic_vector(31 downto 0):=(others=>'0');
        signal unknown_clk_counter					: std_logic_vector(31 downto 0):=(others=>'0');
        signal ratio_divisor                   		: std_logic_vector(63 downto 0):=(others=>'0');
        signal ratio_precision                 		: std_logic_vector(31 downto 0):=x"1000_0000";
        signal ratio_multiplier                     : std_logic_vector(63 downto 0):=(others=>'0');
        signal ratio_trace_counter                  : std_logic_vector(63 downto 0):=(others=>'0');
        signal unknown_freq_value             		: std_logic_vector(31 downto 0):=(others=>'0');
        
        signal start_division                       : std_logic                    := '0';
        
		-- FREQUENCY COMPUTE
		signal alpha							: std_logic_vector( 63 downto 0) := (others => '0');
		signal beta								: std_logic_vector( 63 downto 0) := (others => '0');
		signal delta							: std_logic						 :=  		   '0' ;
		signal phi								: std_logic_vector( 63 downto 0) := (others => '0');
		signal frequency_value					: std_logic_vector( 63 downto 0) := (others => '0');
		
		signal step_counter						: integer						 := 0;
    
        signal reset                            : std_logic                      := '0';
        signal soft_reset                       : std_logic                      := '0';
        signal computation_done                 : std_logic                      := '0';

   --==================================== Component Declaration ====================================--          

   
   --==============================================================================================--  

--=================================================================================================--
begin                 --========####   Architecture Body   ####========-- 
--=================================================================================================--

   --==================================== User Logic ==============================================--   
    reset                       <= RESET_I or soft_reset;
   
   --===================================================================================================--
   --================================# Reference Clock Driven Counter #=================================--
   --===================================================================================================--
  ref_counter_proc:
  process(reset,CLK_REF_I)
	begin
		if reset = '1' then
			ref_clk_counter		<= (others=>'0');
		elsif rising_edge(CLK_REF_I) then
            if start_division = '0' then
                ref_clk_counter		<= ref_clk_counter + '1';
            else
                ref_clk_counter		<= ref_clk_counter;
            end if;
		end if;
	end process;
	
   --===================================================================================================--
   --================================# Unknown Clock Driven Counter #=================================--
   --===================================================================================================--
  unknown_counter_proc:
  process(reset,CLK_UNKNOWN_I)
	begin
		if reset = '1' then
			unknown_clk_counter		<= (others=>'0');
		elsif rising_edge(CLK_UNKNOWN_I) then
            if start_division = '0' then
                unknown_clk_counter		<= unknown_clk_counter + '1';
            else
                unknown_clk_counter		<= unknown_clk_counter;
            end if;
		end if;
	end process;
	
   --===================================================================================================--
   --================================# Ratio Divisor Finder #===============--
   --===================================================================================================--
  latch:
  process(reset,CLK_REF_I)
	begin
		if reset = '1' then
                start_division      <= '0';
		elsif rising_edge(CLK_REF_I) then
			if ref_clk_counter = ratio_precision then
                start_division      <= '1';
			end if;
		end if;
	end process;	
	
    s_REFERENCE_FREQUENCY_VALUE      <= std_logic_vector(to_unsigned(REFERENCE_FREQUENCY_VALUE*1000000,32));
   
   
   --===================================================================================================--
   --================================#  Freq Value Calculator #=====================================--
   --===================================================================================================--	
    
       frequency_compute:
       process(reset,CLK_REF_I)
       begin
            if reset = '1' then
                alpha							                <= (others => '0');
                beta							                <= (others => '0');
                delta							                <=            '0' ;
                phi (31 downto 0 )				                <= (others => '0'); 
                phi	(63 downto 32)				                <= s_REFERENCE_FREQUENCY_VALUE;
                frequency_value					                <= (others => '0'); 
                step_counter					                <=  0 ;
                                
                computation_done			                    <= '0';
                
            elsif rising_edge(CLK_REF_I) then
            
                if start_division = '1' then
                    if step_counter = 0 then
                        alpha						            <=	unknown_clk_counter & x"0000_0000";
                        beta 						            <=  ref_clk_counter     & x"0000_0000";                    
                        step_counter				            <=  step_counter + 1;
                        phi							            <=  s_REFERENCE_FREQUENCY_VALUE & x"0000_0000";
                        frequency_value					        <= (others => '0');
                    else        
                        beta						            <= '0' & beta(63 downto 1);
                        phi 						            <= '0' &  phi(63 downto 1);
                                
                        if  alpha >= beta then      
                            alpha 					            <= alpha - beta;
                            frequency_value				        <= frequency_value + phi;
                        end if;     
                        
                        if beta = x"0000_0000" then
                                computation_done 		        <= '1';
                                CLK_FREQUENCY_VALUE_O           <= frequency_value(63 downto 32);
                        end if;	
                        
                    end if;
                end if;                
            end if;
       end process;
	
        COMPUTATION_DONE_O                                      <= computation_done;

   --===================================================================================================--
   --==========================================#  Auto Reset #==========================================--
   --===================================================================================================--	
    
    auto_reset:
    process(CLK_REF_I)
    begin
        if rising_edge(CLK_REF_I) then
                
                soft_reset                                      <= computation_done;
                
        end if;
    end process;
    

end behavioral;
--=================================================================================================--
--#################################################################################################--
--=================================================================================================--