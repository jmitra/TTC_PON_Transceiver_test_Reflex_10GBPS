--------------==============================================------------------------

This design is used for basic PRBS communication test between Arria 10 and Kintex 7 at 10 Gbps

--------------==============================================------------------------

To exec
clean.sh -- Clears all the directories for fresh start
gen_ip.sh -- If IP generation is needed for both synthesis and simulation (optional)
compile.ute the code from terminal use the script files within Project Folder.
sh -- For Compilation
prog.sh -- To program the Arria 10 board

-------------===============================================-------------------------

To monitor and control the signals use Signal tap (.stp) file  and In-System Sources and Probes files (.spf)
